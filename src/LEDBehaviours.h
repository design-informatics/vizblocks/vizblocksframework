#ifndef LED_BEHAVIOUR_h
#define LED_BEHAVIOUR_h
#include <Arduino.h>
#include "Behaviours.h"

#include <Adafruit_NeoPixel.h>

/** \file LEDBehaviours.h
    \brief Behaviours to control a neopixel ring.
		This should be extended with stable behaviours that are developed in the
		future.
*/

/** \class LightSomeLEDs
    \brief Behaviour that lights up a number of LEDs specified as its argument.
*/
class LightSomeLEDs : public Behaviour
{
	Adafruit_NeoPixel* _strip;
	uint32_t _color;

public:
	LightSomeLEDs(Adafruit_NeoPixel* strip, String name = "LightSome", uint32_t color=0xFFFFFFFF);
	char* args();
	String start(String args);
};

/** \class LightAllLEDs
    \brief Behaviour that lights up all the LEDs to a brightness specified as
		its argument.
*/
class LightAllLEDs : public Behaviour
{
	Adafruit_NeoPixel* _strip;
	uint32_t _hue;
	uint32_t _sat;

public:
	LightAllLEDs(Adafruit_NeoPixel* strip, String name = "LightAll", uint32_t hue=0, uint32_t sat=0);
	char* args();
	String start(String args);

};

/** \class BreatheLEDs
    \brief Behaviour that cycles all LEDs from full to minimum brightness and
		back at a speed specified as its argument.
*/
class BreatheLEDs : public Behaviour
{
	Adafruit_NeoPixel* _strip;
	uint _hue;
	uint _sat;
	int32_t _current = 0;
	 //Allows us to have slightly slower behaviours on the go...
	int _factor = 4;
	int _rate = 0;
	int _direction = 1;

public:
	BreatheLEDs(Adafruit_NeoPixel* strip, String name = "Breathe", uint32_t hue=0, uint32_t sat=0);
	char* args();
	String start(String args);
	void update();

};

#endif
