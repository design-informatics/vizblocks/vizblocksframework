#include "LEDBehaviours.h"

//---

LightSomeLEDs::LightSomeLEDs(Adafruit_NeoPixel* strip, String name, uint32_t color) :
	Behaviour(name), _strip(strip), _color(color)
{ }

String LightSomeLEDs::start(String args)
{
	int val = args.toInt();
	 //Always clear the strip first
	_strip->clear();

	if( val > 0 )
	{
		_strip->fill(_color, 0, val);
	}

	_strip->show();

	return "";
};

char * LightSomeLEDs::args()
{
	return "<int num_leds>";
};

//---

LightAllLEDs::LightAllLEDs(Adafruit_NeoPixel* strip, String name, uint32_t hue, uint32_t sat) :
	Behaviour(name), _strip(strip), _hue(hue), _sat(sat)
{ }

String LightAllLEDs::start(String args)
{
	int val = args.toInt();
	_strip->clear();
	_strip->fill(_strip->ColorHSV(_hue,_sat,val));
	_strip->show();

	return "";
};

char * LightAllLEDs::args()
{
	return "<int brightness>";
};

//---

BreatheLEDs::BreatheLEDs(Adafruit_NeoPixel* strip, String name, uint32_t hue, uint32_t sat) :
	Behaviour(name), _strip(strip), _hue(hue * 255), _sat(sat)
{ }

void BreatheLEDs::update()
{
	if( _rate <= 0 )
	{
		_strip->fill(0);
		_strip->show();

		return;
	}

	_current = _current + (_rate * _direction);

	if( _current < 0 )
	{
		_current = 0;
		_direction = 1;
	}

	if( _current > 255 * _factor )
	{
		_current = 255 * _factor;
		_direction = -1;
	}

	_strip->fill(_strip->ColorHSV(_hue,_sat,_current / _factor));
	_strip->show();

};

String BreatheLEDs::start(String args)
{
	_current = 0;
	_direction = 1;
	_running = true;
	int val = args.toInt();
	_rate = val;

	return "";
};

char * BreatheLEDs::args()
{
	return "<int rate (1-255ish)>";
};
